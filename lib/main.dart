import 'package:flutter/material.dart';
import './app_screens/home.dart';

void main() => runApp(new MyFlutterApp());

class MyFlutterApp extends StatelessWidget  {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Exploring UI Widgets",
      home: Scaffold(
        appBar: AppBar(title: Text("My Flutter App"),
          backgroundColor: Colors.black87,),
        body: Home(),
      )
    );
  }
}