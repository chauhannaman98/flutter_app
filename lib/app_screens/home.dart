import 'package:flutter/material.dart';

class Home extends StatelessWidget  {

  @override
  Widget build(BuildContext context)  {

    return Center(
      child: Container(
        alignment: Alignment.center,
        color: Colors.lightBlueAccent,
        margin: EdgeInsets.all(0.00),
        child: Text("Hello Flutter",
          textDirection: TextDirection.ltr,
          textAlign: TextAlign.center,
          textScaleFactor: 2.00,
        ),
      )
    );
  }
}